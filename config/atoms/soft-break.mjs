export default {
  name: 'soft-break',
  label: 'Soft break',
  iconText: '⏎',
  type: 'dom',
  shortcut: 'SHIFT+ENTER',
  hidden: true,
  render({ env }) {
    const doc = env.dom || document;
    return doc.createElement('br');
  },
};