const color = {};
const textRendering = {};

// TEXT RENDERING
// Use with caution, this may decrease legibility.
// Only set this to antialiased, if the background
// is darker than the text.
textRendering.main = 'antialiased';
textRendering.sidebar = 'antialiased';

// COLORS
// Main
color.mainBackground = 'hsl(0, 0%, 21%)';
color.mainText = 'hsl(0, 0%, 91%)';
color.highlight = 'hsl(346, 82%, 53%)';
color.illustrations = 'hsl(0, 0%, 16%)'; 
color.mainButtonBorder = 'hsl(0, 0%, 33%)';
color.mainButtonBorderHover = 'hsl(0, 0%, 35%)';
color.mainButtonBackgroundActive = 'transparent';

// Sidebar
color.sidebarBackground = 'hsl(0, 0%, 16%)';
color.sidebarEntryText = 'hsl(0, 0%, 91%)';
color.sidebarEntryBackgroundHover = 'hsl(0, 0%, 13%)';
color.sidebarButtonBorder = 'transparent';
color.sidebarButtonBorderHover = 'hsl(0, 0%, 25%)';
color.sidebarButtonBackground = 'transparent';
color.sidebarButtonBackgroundActive = color.sidebarEntryBackgroundHover;
color.sidebarButtonText = color.sidebarEntryText;
color.sidebarDivider = 'hsl(0, 0%, 25%)';

// Search
color.searchBorder = 'hsl(0, 0%, 27%)';
color.searchBackground = color.mainBackground;
color.searchTag = color.sidebarButtonBorderHover;
color.searchTagText = color.sidebarButtonText;
color.searchText = color.sidebarEntryText;
    
// Metadata
color.metaTagBackground = 'hsl(0, 0%, 33%)';
color.metaTagText = color.mainText;
color.metaFieldText = color.mainText;
color.metaFieldBorder = 'transparent';
color.metaFieldBorderHover = color.searchBorder;
    
// Modals
color.modalOverlay = 'hsla(0, 0%, 16%, 0.5)';
color.modalBorder = color.illustrations;

// Menus
color.menuBackground = color.sidebarEntryBackgroundHover;
color.menuEntryText = color.mainText;
color.menuEntryTextActive = color.menuEntryText;
color.menuEntryBackgroundActive = color.mainBackground;
color.menuEntryTextHover = color.mainText;
color.menuEntryBackgroundHover = color.highlight;
color.sectionButton = color.mainText;

export default { color, textRendering };