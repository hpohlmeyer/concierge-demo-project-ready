// Config imports
import editorConfig from './editor/editor.mjs';
import ftpConfig from './ftp/ftp.mjs';

// Template imports
import basicMarkup from './templates/basic-markup.mjs';

// Theme imports
import darkTheme from './themes/dark-theme.mjs';

// Export the main config.
export default {
  generator: {
    // The terminal command used to run the generator
    buildCommand: 'node index.js',
  },
  editor: editorConfig,
  ftp: ftpConfig,
  templates: {
    home: basicMarkup,
  },
  // theme: darkTheme,
};