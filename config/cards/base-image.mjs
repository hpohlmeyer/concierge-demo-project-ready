const DIALOG_OPTIONS = {
  properties: ['openFile'],
  filters: [{ name: 'Images', extensions: ['jpg', 'png', 'gif']}],
};

const PLACEHOLDER_IMG = `
  <svg class="base-img-placeholder" width="702" height="269">
    <path class="base-img-placeholder__background" d="M3 1h696a2 2 0 0 1 2 2v265H1V3a2 2 0 0 1 2-2z"/>
    <rect class="base-img-placeholder__button" width="166" height="52" x="268" y="109" rx="2"/>
    <text class="base-img-placeholder__label">
      <tspan x="351" y="142">Add an image</tspan>
    </text>
  </svg>
`;

async function loadImage({ env, utils, payload }) {
  const { saveAsset, openDialog } = utils;
  
  // Use a dialog to get the file path
  const originalFilePath = await openDialog(DIALOG_OPTIONS);

  // Return if dialog has been canceled
  if (!originalFilePath) { return; }
  
  // Delete old asset
  utils.deleteAsset({ assetPath: payload.fullPath, force: true });

  // Copy the given asset to the project
  const assetPaths = await saveAsset({ filePath: originalFilePath[0] });
  
  // Save the card
  env.save(Object.assign({}, payload, assetPaths), true);
}

function edit({ env, utils, payload }) {
  // Deconstruct utility functions
  const { deleteAsset, parseHtmlString } = utils;

  // Make sure to remove the assets on card teardown
  env.onTeardown((editorTearingDown) => {
    if (editorTearingDown || !payload.fullPath) { return; }
    deleteAsset({ assetPath: payload.fullPath, force: true });
  });

  // Generate the markup
  const src = (payload && payload.fullPath && `file://${payload.fullPath}`) || null;
  const img = src ? `<img src="${src}" />` : PLACEHOLDER_IMG;
  const component = parseHtmlString(`
    <style>
      .card {
        display: flex;
        justify-content: center;
      }
      .wrapper {
        position: relative;
        display: inline-flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
        line-height: 0;
        flex-direction: column;
      }
      .image img {
        display: block;
      }
      .gradient-overlay {
        display: block;
        filter: contrast(85%);
        mix-blend-mode: color;
        background-image: linear-gradient(45deg, var(--color1) 0%, var(--color2) 100%);
        position: absolute;
        top: 0;
        width: 100%;
        height: calc(100% - 50px);
        z-index: 1;
      }
      .image:hover:before {
        position: absolute;
        color: #fff;
        content: "Replace Image";
        display: block;
        padding: 10px 20px;
        border-radius: 3px;
        border: 3px solid #fff;
      }
      .color-inputs {
        display: flex;
        width: 100%;
      }
      .color-chooser {
        -webkit-appearance: none;
        border: 0;
        padding: 0;
        cursor: pointer;
        overflow: hidden;
        width: 50%;
        height: 50px;
      }
      .color-chooser::-webkit-color-swatch {
        border: 0;
      }
      .color-chooser::-webkit-color-swatch-wrapper {
        padding: 0;
      }
      .base-img-placeholder__background {
        fill: var(--color-sidebar-background);
        stroke: var(--color-sidebar-background);
        stroke-width: 2px;
      }
      .base-img-placeholder__button {
        fill: var(--color-sidebar-background);
      }
      .gradient-overlay:hover ~ .base-img-placeholder .base-img-placeholder__button {
        stroke: var(--color-sidebar-button-border-hover);
        stroke-width: 2px;
      }
      .base-img-placeholder__label {
        fill: var(--color-sidebar-text);
        font-family: var(--fonts-ui);
        text-anchor: middle;
      }
    </style>

    <div class="card">
      <div class="wrapper">
        <div class="gradient-overlay"></div>
        ${img}
        <div class="color-inputs">
          <input class="color-chooser" type="color" value="${payload.color1 || '#000000'}">
          <input class="color-chooser" type="color" value="${payload.color2 || '#000000'}">
        </div>
      </div>
    </div>
  `);

  // Set inital gradient colors
  const gradientElem = component.querySelector('.gradient-overlay');
  gradientElem.style.setProperty('--color1', payload.color1 || '#000000');
  gradientElem.style.setProperty('--color2', payload.color2 || '#000000');
  gradientElem.addEventListener('click', () => loadImage({ env, utils, payload }));

  // Enable the color choosers to change the gradient.
  // The change event is buggy in Chrome and should only fire if the input modal has been closed.
  const colorChoosers = component.querySelectorAll('input');
  colorChoosers.forEach((elem, index) => {
    elem.addEventListener('input', evt => gradientElem.style.setProperty(`--color${index+1}`, evt.target.value));
    elem.addEventListener('change', evt => env.save(Object.assign({}, payload, { [`color${index+1}`]: evt.target.value })));
  });

  // Export the markup
  // This has to be a single DOM node
  // (Mobiledoc DOM Parser requirement)
  const markup = document.createElement('div');
  markup.appendChild(component);
  return markup;
}

function render({ env, payload }) {
  if (!(payload && payload.renderPath)) { return false; }
  
  // Create the card
  const card = env.dom.createElement('div');
  card.setAttribute('class', 'card');

  // Create the color overlay
  const overlay = env.dom.createElement('div');
  overlay.setAttribute('class', 'gradient-overlay');
  const color1 = payload.color1 || 'black';
  const color2 = payload.color2 || 'black';
  overlay.setAttribute('style', `--color1: ${color1}; --color2: ${color2};`);
  card.appendChild(overlay);
  
  // Create the image
  const img = env.dom.createElement('img');
  img.setAttribute('src', payload.renderPath);
  card.appendChild(img);

  // Return the card DOM
  return card;
}

export default {
  name: 'base-image',
  label: 'Image',
  iconText: 'i',
  type: 'dom',
  render,
  edit,
};