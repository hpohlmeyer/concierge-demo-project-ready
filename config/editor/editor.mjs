import BaseImageCard from '../cards/base-image.mjs';
import SoftBreakAtom from '../atoms/soft-break.mjs';

export default {
  atoms: [SoftBreakAtom],
  cards: [BaseImageCard],
  sections: [
    { name: 'headline1', label: 'Headline 1', iconText: 'H1', tag: 'h1' },
    { name: 'headline2', label: 'Headline 2', iconText: 'H2', tag: 'h2' },
    { name: 'headline3', label: 'Headline 3', iconText: 'H3', tag: 'h3' },
    { name: 'headline4', label: 'Headline 4', iconText: 'H4', tag: 'h4' },
    { name: 'headline5', label: 'Headline 5', iconText: 'H5', tag: 'h5' },
    { name: 'blockquote', label: 'Blockquote', iconText: '”', tag: 'blockquote' },
  ],
  markup: [
    { name: 'bold', label: 'Bold', icon: 'bold', iconText: 'b', tag: 'strong', shortcut: 'META+B' },
    { name: 'italic', label: 'Italic', icon: 'italic', iconText: 'i', tag: 'em', shortcut: 'META+I' },
    { name: 'underline', label: 'Underline', icon: 'underline', iconText: 'u', tag: 'u', shortcut: 'META+U' },
    { name: 'strike-through', label: 'Strike Through', icon: 'strikethrough', iconText: 's', tag: 's', shortcut: 'META+-' },
  ],
};