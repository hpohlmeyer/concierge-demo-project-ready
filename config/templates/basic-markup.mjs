export default {
  modules: [
    {
      allowedAtoms: ['soft-break'],
      allowedCards: [],
      allowedMarkup: ['bold', 'italic'],
      allowedSections: ['headline1', 'headline2', 'blockquote'],
    },
  ],
};